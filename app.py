"""
API de integração com Facebook para ler dados usuários.
"""

from core.auth import criar_token, autenticacao, get_public_profile
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import FastAPI, Depends, HTTPException
from core import crud, schemas, models
from sqlalchemy.orm import Session
from core.config import get_db
from core.db import engine
from core import config


models.Base.metadata.create_all(bind=engine)

api = FastAPI(title="API de Integração com Facebook")


@api.post("/accounts/create", response_model=schemas.AccountResponse, status_code=201)
def criar(account: schemas.Account, db: Session = Depends(get_db)):
    """Cria uma conta."""
    user = db.query(models.Account).filter(
        models.Account.email == account.email
    ).first()
    if user:
        raise HTTPException(status_code=400, detail="Email já está cadastrado.")
    return crud.create(db, account)

@api.post("/accounts/login")
def login(login_data: schemas.Login, db: Session = Depends(get_db)):
    """Endpoint de login e consulta do 'public_profile' no Facebook."""
    user = autenticacao(email=login_data.email, password=login_data.password, db=db)
    if user is None:
        raise HTTPException(status_code=400, detail="Credênciais inválidas")
    return {
        "access_token": criar_token(config.TOKEN_TYPE, config.ACCESS_TOKEN_EXPIRE_MINUTES, user),
        "token_type": "bearer",
        "user": user,
        "public_profile": get_public_profile(login_data.user_id, user.token)
    }

@api.put("/accounts/{id}/update")
def atualizar(id: int, account: schemas.Account, db: Session = Depends(get_db)):
    """Atualiza dos dados de uma conta"""
    account = crud.update(db, id, account)
    if not account:
        raise HTTPException(status_code=404, detail=f"Conta com o id:{id} não encontrada")
    return account

@api.delete("/accounts/{id}/delete")
def apagar(id: int, db: Session = Depends(get_db)):
    """Apaga uma conta especifica"""
    account = crud.delete(db, id)
    if not account:
        raise HTTPException(status_code=404, detail=f"Conta com o id:{id} não encontrada")
    return account

@api.get("/accounts/list")
def listar(db: Session = Depends(get_db)):
    """Lista todas as contas cadastradas."""
    accounts = crud.all(db)
    return accounts
