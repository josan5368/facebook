from typing import Optional
from pydantic import BaseModel


class Account(BaseModel):
    nome: str
    email: str
    senha: str
    token: str
    
    class Config:
        orm_mode = True


class AccountResponse(BaseModel):
    id: int
    nome: str
    email: str

    
    class Config:
        orm_mode = True

class Login(BaseModel):
    user_id: str
    email: str
    password: str