"""
"""

from sqlalchemy import Column, String, Integer


from core.db import Base


class Account(Base):

    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True, index=True)
    nome = Column(String(255))
    email = Column(String, unique=True, index=True, nullable=False)
    password = Column(String, nullable=False)
    token = Column(String, nullable=False)
