"""
Base de configurações.
"""

from core.db import SessionLocal
from decouple import config


FB_GRAPH_DOMAIN: str = "https://graph.facebook.com"
FB_GRAPH_VERSION: str = "v12.0"
FB_URL: str = f"{FB_GRAPH_DOMAIN}/{FB_GRAPH_VERSION}"

TOKEN_TYPE: str = "bearer"
JWT_SECRET: str = "TEST_SECRET_DO_NOT_USE_IN_PROD"
ALGORITHM: str = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 3


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
