import requests
from datetime import datetime, timedelta

from fastapi.security import OAuth2PasswordBearer
from fastapi.exceptions import HTTPException
from fastapi import status, Depends
from pydantic.main import BaseModel
from sqlalchemy.orm import Session
from core.models import Account
from jose import jwt, JWTError

from core.security import check_password
from core.models import Account
from core import config


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/accounts/login")


class TokenData(BaseModel):
    pass


def autenticacao(email: str, password: str, db: Session):
    user =  db.query(Account).filter(
        Account.email == email
    ).first()
    if not user:
        return None
    if not check_password(password, user.password):
        return None
    return user

def criar_token(token_type: str, lifetime: timedelta, sub: str):
    payload = {}
    expire = datetime.utcnow() + timedelta(minutes=config.ACCESS_TOKEN_EXPIRE_MINUTES)
    payload["type"] = token_type
    payload["exp"] = expire  
    payload["iat"] = datetime.utcnow() 
    payload["sub"] = str(sub)  
    return jwt.encode(payload, config.JWT_SECRET, algorithm=config.ALGORITHM)

def get_public_profile(user_id, access_token):
    res = requests.get(f"{config.FB_URL}/{user_id}", params={
        'access_token': access_token,
        'fields': 'id,name,picture,first_name,last_name,middle_name,short_name,name_format'
    }).json()
    return res
