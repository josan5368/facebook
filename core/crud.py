from core.security import generate_hash_password
from sqlalchemy.orm import Session
from core import models, schemas


def create(db: Session, data: schemas.Account):
    """Criar nova conta."""
    user_data = data.dict()
    user_data.pop('senha')
    account = models.Account(**user_data)
    account.password = generate_hash_password(data.senha)
    db.add(account)
    db.commit()
    db.refresh(account)
    return account

def read(db: Session, account_id: int):
    account = db.query(models.Account).get(account_id) 
    return account

def update(db: Session, account_id: int, data: schemas.Account):
    """Atualiza as informações da conta."""
    account = db.query(models.Account).filter(models.Account.id == account_id).first()
    if account:
        account.nome = data.nome
        account.email = data.email
        account.password = generate_hash_password(data.senha)
        account.token = data.token
        db.commit()
        db.refresh(account)
        return account  
    
def delete(db: Session, account_id: int):
    """Apaga uma determina conta"""
    account = db.query(models.Account).get(account_id)
    if account:
        db.delete(account)
        db.commit()
        db.close()
        return {'id': account_id, 'delete': True}

def all(db: Session):
    """Lista todas as contas criadas."""
    accounts = db.query(models.Account).all()
    return accounts
