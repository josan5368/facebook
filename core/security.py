from passlib.context import CryptContext

PWD_CONTEXT = CryptContext(schemes=["bcrypt"], deprecated="auto")

def generate_hash_password(password: str) -> str: 
    return  PWD_CONTEXT.hash(password)

def check_password(password1: str, password2: str) -> bool:
    return PWD_CONTEXT.verify(password1, password2)